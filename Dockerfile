FROM youmoni/jdk8

ENV TZ=Asia/Shanghai

WORKDIR /app

ENTRYPOINT ["java","-jar","app.jar"]
