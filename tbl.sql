drop table authplatform.tbl_app;
create table authplatform.tbl_app
(
    id          bigint                                 not null primary key,
    group_id    bigint       default 0                 not null,
    app_name    varchar(100) default ''                not null,
    server_ip   varchar(100) default ''                not null,
    server_port int4         default 0                 not null,
    front_ip    varchar(100) default ''                not null,
    front_port  int4         default 0                 not null,
    active_rule varchar(255) default ''                not null,
    create_time timestamp    default CURRENT_TIMESTAMP not null
);

comment on table authplatform.tbl_app is '应用表';
comment on column authplatform.tbl_app.group_id is '小组ID';
comment on column authplatform.tbl_app.app_name is '应用名称';
comment on column authplatform.tbl_app.server_ip is '服务端IP';
comment on column authplatform.tbl_app.server_port is '服务端端口';
comment on column authplatform.tbl_app.front_ip is '前端IP';
comment on column authplatform.tbl_app.front_port is '前端端口';
comment on column authplatform.tbl_app.active_rule is '根路由';
comment on column authplatform.tbl_app.create_time is '创建时间';

drop table authplatform.tbl_app_user;
create table authplatform.tbl_app_user
(
    id            bigint not null primary key,
    uid           bigint    default 0                 not null,
    app_id        bigint    default 0                 not null,
    group_id      bigint    default 0                 not null,
    group_role_id bigint    default 0                 not null,
    create_time   timestamp default CURRENT_TIMESTAMP not null
);

comment on table authplatform.tbl_app_user is '应用用户表';
comment on column authplatform.tbl_app_user.uid is '用户ID';
comment on column authplatform.tbl_app_user.app_id is '应用ID';
comment on column authplatform.tbl_app_user.group_id is '小组ID';
comment on column authplatform.tbl_app_user.group_role_id is '小组角色ID';
comment on column authplatform.tbl_app_user.create_time is '创建时间';

drop table authplatform.tbl_group;
create table authplatform.tbl_group
(
    id          bigint not null primary key,
    group_name  varchar(100) default ''                not null,
    create_time timestamp    default CURRENT_TIMESTAMP not null
);

comment on table authplatform.tbl_group is '小组';
comment on column authplatform.tbl_group.group_name is '小组名称';
comment on column authplatform.tbl_group.create_time is '创建时间';

drop table authplatform.tbl_group_role;
create table authplatform.tbl_group_role
(
    id         bigint not null primary key,
    group_id   bigint       default 0  not null,
    role_name  varchar(100) default '' not null,
    role_level int4         default 0  not null
);

comment on table authplatform.tbl_group_role is '小组角色';
comment on column authplatform.tbl_group_role.group_id is '小组ID';
comment on column authplatform.tbl_group_role.role_name is '角色名称';
comment on column authplatform.tbl_group_role.role_level is '角色等级';

drop table authplatform.tbl_group_user;
create table authplatform.tbl_group_user
(
    id            bigint not null primary key,
    group_id      bigint    default 0                 not null,
    uid           bigint    default 0                 not null,
    group_role_id bigint    default 0                 not null,
    create_time   timestamp default CURRENT_TIMESTAMP not null
);

comment on table authplatform.tbl_group_user is '小组用户';
comment on column authplatform.tbl_group_user.group_id is '小组ID';
comment on column authplatform.tbl_group_user.uid is '用户ID';
comment on column authplatform.tbl_group_user.group_role_id is '角色ID';
comment on column authplatform.tbl_group_user.create_time is '创建时间';

drop table authplatform.tbl_menu;
create table authplatform.tbl_menu
(
    id        bigint not null primary key,
    app_id    bigint       default 0  not null,
    title     varchar(100) default '' not null,
    key_id    varchar(100) default '' not null,
    path_url  varchar(255) default '' not null,
    parent_id bigint       default 0  not null
);

comment on table authplatform.tbl_menu is '左侧菜单配置';
comment on column authplatform.tbl_menu.app_id is '应用ID';
comment on column authplatform.tbl_menu.title is '标题';
comment on column authplatform.tbl_menu.key_id is '唯一标识Key';
comment on column authplatform.tbl_menu.path_url is '路径';
comment on column authplatform.tbl_menu.parent_id is '父级ID';

drop table authplatform.tbl_user_menu;
create table authplatform.tbl_user_menu
(
    id      bigint not null primary key,
    app_id  bigint default 0 not null,
    uid     bigint default 0 not null,
    menu_id bigint default 0 not null
);

comment on table authplatform.tbl_user_menu is '用户菜单授权';
comment on column authplatform.tbl_user_menu.app_id is '应用ID';
comment on column authplatform.tbl_user_menu.uid is '用户ID';
comment on column authplatform.tbl_user_menu.menu_id is '菜单ID';

--ALTER TABLE authplatform.tbl_menu DROP COLUMN height;
--ALTER TABLE authplatform.tbl_user_menu DROP COLUMN height;

create table authplatform.tbl_user
(
    id          bigint                                 not null primary key,
    uname       varchar(100) default ''                not null,
    passwd      varchar(100) default ''                not null,
    name        varchar(30)  default ''                not null,
    sex         int2         default 0                 not null,
    sex_name    varchar(10)  default ''                not null,
    job_title   varchar(100) default ''                not null,
    mobile      varchar(100) default ''                not null,
    email       varchar(100) default ''                not null,
    role_level  int4         default 0                 not null,
    create_time timestamp    default CURRENT_TIMESTAMP not null
);

comment on table authplatform.tbl_user is '用户信息';
comment on column authplatform.tbl_user.uname is '用户名称';
comment on column authplatform.tbl_user.passwd is '用户密码';
comment on column authplatform.tbl_user.name is '用户姓名';
comment on column authplatform.tbl_user.sex is '性别';
comment on column authplatform.tbl_user.sex_name is '姓名名称';
comment on column authplatform.tbl_user.job_title is '职称';
comment on column authplatform.tbl_user.mobile is '手机号';
comment on column authplatform.tbl_user.email is '邮箱';
comment on column authplatform.tbl_user.role_level is '用户等级';
comment on column authplatform.tbl_user.create_time is '创建时间';


