logs:
	docker logs -f --tail=300 msun_auth_platform

top:
	top $(ps -e | grep java | awk '{print $1}' | sed 's/^/-p/')

restart:
	docker-compose restart

git:
	git status
	git add .
	git commit -m "gj"
	git push

buildProd:
	mvn clean install package -P prod -Dmaven.test.skip=true

buildTest:
	mvn clean install package -P test -Dmaven.test.skip=true

buildDev:
	mvn clean install package -P dev -Dmaven.test.skip=true