package com.msun.auth.utils.interceptor;

import lombok.Data;

/**
 * 类描述：
 *
 * @ClassName AuthContext
 * @Description TODO
 * @Author gj
 * @Date 2023/8/23 21:12
 * @Version 1.0
 */

public class AuthContext {
    @Data
    public static class Context {
        private String ip;
        private Long uid = 0L;
        private String uname = "";
        private String name = "";
        private Long groupId = 0L;
        private Long appId = 0L;
        private Integer roleLevel = 0;
    }

    private static final ThreadLocal<Context> context = new ThreadLocal<>();

    public static void set(Context ctx) {
        context.set(ctx);
    }

    public static Context get() {
        return context.get();
    }

    public static void remove() {
        context.remove();
    }
}