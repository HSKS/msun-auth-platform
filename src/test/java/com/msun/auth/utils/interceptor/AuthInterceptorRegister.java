package com.msun.auth.utils.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 类描述：
 *
 * @ClassName AuthInterceptorRegister
 * @Description TODO
 * @Author gj
 * @Date 2023/8/23 21:14
 * @Version 1.0
 */

@Configuration
public class AuthInterceptorRegister  implements WebMvcConfigurer {
    private final AuthInterceptor authInterceptor;

    @Autowired
    public AuthInterceptorRegister(AuthInterceptor authInterceptor) {
        this.authInterceptor = authInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).addPathPatterns("/**");
    }
}