package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 类描述：
 *
 * @ClassName AppUserMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:27
 * @Version 1.0
 */
@Data
@TableName(value = "tbl_app_user")
public class AppUserMo {
    @TableId
    private Long id;
    private Long uid;
    private Long appId;
    private Long groupId;
    private Long groupRoleId;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}