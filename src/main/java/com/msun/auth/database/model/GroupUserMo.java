package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 类描述：
 *
 * @ClassName GroupUserMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:27
 * @Version 1.0
 */
@Data
@TableName(value = "tbl_group_user")
public class GroupUserMo {
    @TableId
    private Long id;
    private Long groupId;
    private Long uid;
    private Long groupRoleId;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}