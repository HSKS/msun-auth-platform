package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * 类描述：
 *
 * @ClassName MenuMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:26
 * @Version 1.0
 */
@Data
@TableName(value = "tbl_menu")
public class MenuMo {
    @TableId
    private Long id;
    private Long appId;
    private String title;
    private String keyId;
    private String pathUrl;
    private Long parentId = 0L;
    @TableField(exist = false)
    private List<MenuMo> children;
    @TableField(exist = false)
    private Boolean selected;
}