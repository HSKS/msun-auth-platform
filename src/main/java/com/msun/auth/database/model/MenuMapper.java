package com.msun.auth.database.model;

import com.msun.auth.database.conf.RootMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuMapper extends RootMapper<MenuMo> {
    List<MenuMo> adminGetUserMenuList(@Param("appId") Long appId, @Param("uid") Long uid, @Param("parentId") Long parentId);
    List<MenuMo> openGetAppMenuList(@Param("appId") Long appId, @Param("uid") Long uid, @Param("parentId") Long parentId);
}
