package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * @Description //TODO
 * @Date 2023/8/18 19:23
 * @Author guijie
 **/
@Data
@TableName(value = "tbl_user")
public class UserMo {
    @TableId
    private Long id;
    private String uname;
    private String passwd;
    private String name;
    private Integer sex;
    private String sexName;
    private String jobTitle;
    private String mobile;
    private String email;
    private Integer roleLevel;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
