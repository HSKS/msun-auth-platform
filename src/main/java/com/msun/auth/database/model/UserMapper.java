package com.msun.auth.database.model;

import com.msun.auth.database.conf.RootMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends RootMapper<UserMo> {
}
