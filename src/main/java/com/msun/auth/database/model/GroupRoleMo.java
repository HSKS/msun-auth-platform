package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/**
 * 类描述：
 *
 * @ClassName GroupRoleMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:26
 * @Version 1.0
 */

@Data
@TableName(value = "tbl_group_role")
public class GroupRoleMo {
    @TableId
    private Long id;
    private Long groupId;
    private String roleName;
    private Integer roleLevel;
}