package com.msun.auth.database.model;

import com.msun.auth.database.conf.RootMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GroupUserMapper extends RootMapper<GroupUserMo> {
    List<UserMo> getUserList(@Param("groupId") Long groupId, @Param("offset") Integer offset, @Param("pageSize") Integer pageSize, @Param("groupRoleId") Long groupRoleId, @Param("keyword") String keyword);

    Integer getUserCount(@Param("groupId") Long groupId, @Param("groupRoleId") Long groupRoleId, @Param("keyword") String keyword);
}
