package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 类描述：
 *
 * @ClassName AppMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:24
 * @Version 1.0
 */
@Data
@TableName(value = "tbl_app")
public class AppMo {
    @TableId
    private Long id;
    private Long groupId;
    private String appName;
    private String serverIp;
    private Integer serverPort;
    private String frontIp;
    private Integer frontPort;
    private String activeRule;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}