package com.msun.auth.database.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 类描述：
 *
 * @ClassName GroupMo
 * @Description TODO
 * @Author gj
 * @Date 2023/8/20 21:25
 * @Version 1.0
 */
@Data
@TableName(value = "tbl_group")
public class GroupMo {
    @TableId
    private Long id;
    private String groupName;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(exist = false)
    private List<AppMo> appList;
}