package com.msun.auth.database.model;

import com.msun.auth.database.conf.RootMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AppUserMapper extends RootMapper<AppUserMo> {
    List<UserMo> getUserList(@Param("appId") Long appId, @Param("offset") Integer offset,@Param("pageSize") Integer pageSize,  @Param("keyword") String keyword);

    Integer getUserCount(@Param("appId") Long appId, @Param("keyword") String keyword);
}
