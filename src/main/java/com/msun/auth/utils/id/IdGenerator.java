package com.msun.auth.utils.id;

import com.msun.auth.utils.id.impl.SnowflakeUpperGenerator;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * @description: ID生成器
 * @fileName: IdGenerator.java
 * @author: jason
 * @createAt: 2020/4/8 9:57 上午
 * @updateBy: jason
 * @remark: Copyright
 */
@Slf4j
public class IdGenerator {

    public static final class IdGeneratorHolder {
        public static final IdGenerator GENERATOR = new IdGenerator();
    }

    public static IdGenerator ins() {
        return IdGeneratorHolder.GENERATOR;
    }

    private IdGeneratorInterface defaultGenerator;

    public IdGenerator() {
        Random random = new Random();
        long randomServiceId = random.nextInt(256);
        SnowflakeUpperGenerator defaultGenerator = new SnowflakeUpperGenerator(randomServiceId);
        this.defaultGenerator = defaultGenerator;
    }

    /**
     * 生成唯一ID
     *
     * @return
     */
    public Long generator() {
        Long generator = defaultGenerator.generator();
        return generator;
    }


    public static void main(String[] args) {
        Long id = IdGenerator.ins().generator();
        System.out.println(id);
    }
}
