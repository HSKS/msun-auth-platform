package com.msun.auth.utils.id;

/**
 * @description: ID 生成器
 * @fileName: IdentityGenerator.java
 * @author: jason
 * @createAt: 2020/4/8 9:10 上午
 * @updateBy: jason
 * @remark: Copyright
 */
public interface IdGeneratorInterface {


    /**
     * 生成UID
     *
     * @return
     */
    Long generator();

}
