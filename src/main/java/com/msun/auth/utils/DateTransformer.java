package com.msun.auth.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public final class DateTransformer {

    public static int  getSecond() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.SECOND);
    }

    public static int  getMinute() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MINUTE);
    }

    public static int  getHour() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String getConstellation(long birthday) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("Mdd");
        Date birthdayDate = new Date(birthday);
        String format = simpleDateFormat.format(birthdayDate);
        int date = Integer.parseInt(format);
        if (date >= 121 && date <= 219) {
            return "水瓶座";
        } else if (date >= 220 && date <= 320) {
            return "双鱼座";
        } else if (date >= 321 && date <= 420) {
            return "白羊座";
        } else if (date >= 421 && date <= 521) {
            return "金牛座";
        } else if (date >= 522 && date <= 621) {
            return "双子座";
        } else if (date >= 622 && date <= 722) {
            return "巨蟹座";
        } else if (date >= 723 && date <= 823) {
            return "狮子座";
        } else if (date >= 824 && date <= 923) {
            return "处女座";
        } else if (date >= 924 && date <= 1023) {
            return "天秤座";
        } else if (date >= 1024 && date <= 1122) {
            return "天蝎座";
        } else if (date >= 1123 && date <= 1221) {
            return "射手座";
        } else {
            return "魔蝎座";
        }
    }

    public static Long[] getCurrentMonthFirstAndLastDayTimestamp() {
        //Calendar 是java.util下的工具类
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0); //获取当前月第一天
        c.set(Calendar.DAY_OF_MONTH, 1); //设置为1号,当前日期既为本月第一天
        c.set(Calendar.HOUR_OF_DAY, 0); //将小时至0
        c.set(Calendar.MINUTE, 0); //将分钟至0
        c.set(Calendar.SECOND, 0); //将秒至0
        c.set(Calendar.MILLISECOND, 0); //将毫秒至0

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
        c2.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        c2.set(Calendar.MINUTE, 59); //将分钟至59
        c2.set(Calendar.SECOND, 59); //将秒至59
        c2.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return new Long[]{c.getTimeInMillis(), c2.getTimeInMillis()};
    }

    public static Date getCurrentMonthFirstDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getCurrentMonthLastDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59); //将秒至59
        calendar.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return calendar.getTime();
    }

    public static Date getPrevWeekFirstDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getActualMinimum(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -6);
        return calendar.getTime();
    }

    public static Date getPrevWeekCurrentDayDateTime() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, -7);
        return calendar.getTime();
    }

    public static Date getCurrentWeekFirstDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getActualMinimum(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.HOUR_OF_DAY, 0); //将小时至23
        calendar.set(Calendar.MINUTE, 0); //将分钟至59
        calendar.set(Calendar.SECOND, 0); //将秒至59
        calendar.set(Calendar.MILLISECOND, 0); //将毫秒至999
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public static Date getCurrentWeekLastDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getActualMaximum(Calendar.DAY_OF_WEEK)); //获取当前月最后一天
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59); //将秒至59
        calendar.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return calendar.getTime();
    }

    public static Date getPrevNDaysFirstDateTime(int n) {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -1 * n);
        return calendar.getTime();
    }

    public static Date getPrevNDaysLastDateTime(int n) {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.add(Calendar.DATE, -1 * n);
        return calendar.getTime();
    }

    public static Date getPrevDayFirstDateTime() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0); //将毫秒至0
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getPrevDayLastDateTime() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getCurrentDayFirstDateTime() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0); //将毫秒至0
        return calendar.getTime();
    }

    public static Date getCurrentDayLastDateTime() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return calendar.getTime();
    }

    public static String fmtTimestamp(long millis) {
        return DateFormatUtils.format(millis, "yyyy年MM月dd日 HH:mm:ss");
    }

    public static String YMDHMS(long millis) {
        return DateFormatUtils.format(millis, "yyyyMMddHHmmss");
    }


    public static long diffDays(Date startDate, Date endDate) {
        return ChronoUnit.DAYS.between(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }


    /**
     * 根据用户生日精确计算年龄
     * 用Calender对象取得当前日期对象--从对象中分别取出年月日
     *
     * @author Administrator
     */
    public static int getAgeByBirth(Date birthday) {
        //Calendar：日历
        /*从Calendar对象中或得一个Date对象*/
        Calendar cal = Calendar.getInstance();
        /*把出生日期放入Calendar类型的bir对象中，进行Calendar和Date类型进行转换*/
        Calendar bir = Calendar.getInstance();
        bir.setTime(birthday);
        /*如果生日大于当前日期，则抛出异常：出生日期不能大于当前日期*/
        if (cal.before(birthday)) {
            throw new IllegalArgumentException("The birthday is before Now,It's unbelievable");
        }
        /*取出当前年月日*/
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayNow = cal.get(Calendar.DAY_OF_MONTH);
        /*取出出生年月日*/
        int yearBirth = bir.get(Calendar.YEAR);
        int monthBirth = bir.get(Calendar.MONTH);
        int dayBirth = bir.get(Calendar.DAY_OF_MONTH);
        /*大概年龄是当前年减去出生年*/
        int age = yearNow - yearBirth;
        /*如果出当前月小与出生月，或者当前月等于出生月但是当前日小于出生日，那么年龄age就减一岁*/
        if (monthNow < monthBirth || (monthNow == monthBirth && dayNow < dayBirth)) {
            age--;
        }
        return age;
    }

    public static Date getNYearsAgo(int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR);
        calendar.set(Calendar.YEAR, year - n);
        return calendar.getTime();
    }
}
