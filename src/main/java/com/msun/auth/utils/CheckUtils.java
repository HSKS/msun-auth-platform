package com.msun.auth.utils;

/**
 * @Description //TODO
 * @Date 2023/8/24 14:44
 * @Author guijie
 **/
public final class CheckUtils {
    public static boolean isZero(Number num) {
        if (num == null || num.doubleValue() == 0) {
            return true;
        }
        return false;
    }

    public static boolean notZero(Number num) {
        return !isZero(num);
    }
}
