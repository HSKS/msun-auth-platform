package com.msun.auth.utils;


import cn.hutool.core.collection.CollUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ListUtils {

    /**
     * 将一个大List分批成N个小List
     *
     * @param source    源List
     * @param stageSize 每批大小
     * @param cb        回调函数
     * @param <T>
     */
    public static <T> void chunked(List<T> source, int stageSize, Consumer<List<T>> cb) {
        int total = source.size();
        List<T> arr = null;
        for (int i = 0; i < total; i++) {
            if (i % stageSize == 0) {
                if (CollUtil.isNotEmpty(arr)) {
                    cb.accept(arr);
                }
                arr = new ArrayList<>();
            }
            arr.add(source.get(i));
        }
        if (CollUtil.isNotEmpty(arr)) {
            cb.accept(arr);
        }
    }

    public static <T> List<T> reverse(List<T> source) {
        List<T> arr = new ArrayList<>();
        int total = source.size();
        for (int i = total - 1; i >= 0; i--) {
            arr.add(source.get(i));
        }
        return arr;
    }
}
