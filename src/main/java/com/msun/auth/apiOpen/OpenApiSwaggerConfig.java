package com.msun.auth.apiOpen;

import com.msun.auth.custom.constants.CustomConsts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class OpenApiSwaggerConfig {

    private final String appName = "OPEN-API";

    @Bean(name = "openDoc")
    public Docket openDoc() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(appInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.msun.auth.apiOpen.handler"))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(Collections.singletonList(
                        new ParameterBuilder()
                                .name(CustomConsts.TOKEN)
                                .description("token令牌/登陆的时候不需要传")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(false)
                                .build()
                )).groupName(appName); // 分组
    }

    private ApiInfo appInfo() {
        String gatewayHost = "http://localhost";
        String srvVersion = "1.0.0";
        String orgName = appName;
        String orgEmail = "guijie@msun.com";
        return new ApiInfoBuilder()
                .title(appName)
                .description(appName)
                .termsOfServiceUrl(gatewayHost + "/doc.html")
                .contact(new Contact(orgName, gatewayHost, orgEmail))
                .version(srvVersion)
                .build();
    }

}