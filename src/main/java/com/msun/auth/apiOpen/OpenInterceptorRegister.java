package com.msun.auth.apiOpen;

import com.msun.auth.custom.packdto.ResponseResultInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class OpenInterceptorRegister implements WebMvcConfigurer {


    private final OpenContextInterceptor openContextInterceptor;
    private final ResponseResultInterceptor responseResultInterceptor;

    @Autowired
    public OpenInterceptorRegister(ResponseResultInterceptor responseResultInterceptor, OpenContextInterceptor openContextInterceptor) {
        this.responseResultInterceptor = responseResultInterceptor;
        this.openContextInterceptor = openContextInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(responseResultInterceptor).addPathPatterns("/**/open/**");
        registry.addInterceptor(openContextInterceptor).addPathPatterns("/**/open/**");
    }
}
