package com.msun.auth.apiOpen.cache;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.msun.auth.custom.annotation.CacheExpire;
import com.msun.auth.database.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * 类描述：
 *
 * @ClassName CacheSvc
 * @Description TODO
 * @Author gj
 * @Date 2023/8/19 13:14
 * @Version 1.0
 */

@Service
public class CacheSvc {

    @Autowired
    private AppUserMapper appUserMapper;
    @Autowired
    private GroupRoleMapper groupRoleMapper;
    @Autowired
    private MenuMapper menuMapper;


//    @Cacheable(cacheNames = "AuthCache", keyGenerator = "keyGenerator", sync = true)
//    @CacheExpire(expire = 600)
    public Integer getAppUserRoleLevel(Long appId, Long uid) {
        AppUserMo appUser = new LambdaQueryChainWrapper<>(appUserMapper)
                .eq(AppUserMo::getAppId, appId)
                .eq(AppUserMo::getUid, uid)
                .one();
        if (appUser == null) {
            return -1;
        }
        GroupRoleMo groupRole = new LambdaQueryChainWrapper<>(groupRoleMapper)
                .eq(GroupRoleMo::getId, appUser.getGroupRoleId())
                .one();
        if (groupRole == null) {
            return -1;
        }
        return groupRole.getRoleLevel();
    }

//    @Cacheable(cacheNames = "AuthCache", keyGenerator = "keyGenerator", sync = true)
//    @CacheExpire(expire = 600)
    public List<MenuMo> getAppUserMenuList(Long appId, Long uid) {
        return recursionGetUserMenuList(appId, uid, 0L);
    }

    private List<MenuMo> recursionGetUserMenuList(Long appId, Long uid, Long parentId) {
        List<MenuMo> menus = menuMapper.openGetAppMenuList(appId, uid, parentId);
        if (CollUtil.isEmpty(menus)) {
            return Collections.emptyList();
        }
        for (MenuMo menu : menus) {
            menu.setChildren(recursionGetUserMenuList(appId, uid, menu.getId()));
        }
        return menus;
    }

}