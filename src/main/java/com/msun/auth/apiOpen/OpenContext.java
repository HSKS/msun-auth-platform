package com.msun.auth.apiOpen;

import lombok.Data;

public class OpenContext {

    @Data
    public static class Context {
        private Long groupId = 0L;
        private Long appId = 0L;
        private String ip;
        private Long uid = 0L;
        private String uname = "";
        private String name = "";
        private Integer roleLevel = 0;
    }

    private static final ThreadLocal<Context> context = new ThreadLocal<>();

    public static void set(Context ctx) {
        context.set(ctx);
    }

    public static Context get() {
        return context.get();
    }

    public static void remove() {
        context.remove();
    }
}
