package com.msun.auth.apiAdmin;

import com.msun.auth.custom.packdto.ResponseResultInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AdminInterceptorRegister implements WebMvcConfigurer {


    private final AdminContextInterceptor adminContextInterceptor;
    private final ResponseResultInterceptor responseResultInterceptor;

    @Autowired
    public AdminInterceptorRegister(ResponseResultInterceptor responseResultInterceptor, AdminContextInterceptor adminContextInterceptor) {
        this.responseResultInterceptor = responseResultInterceptor;
        this.adminContextInterceptor = adminContextInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(responseResultInterceptor).addPathPatterns("/**/admin/**");
        registry.addInterceptor(adminContextInterceptor).addPathPatterns("/**/admin/**");
    }
}
