package com.msun.auth.apiAdmin;

import lombok.Data;

public class AdminContext {

    @Data
    public static class Context {
        private String ip;
        private Long uid = 0L;
        private String uname = "";
        private String name = "";
        private Long groupId = 0L;
        private Long appId = 0L;
        private Integer roleLevel = 0;
    }

    private static final ThreadLocal<Context> context = new ThreadLocal<>();

    public static void set(Context ctx) {
        context.set(ctx);
    }

    public static Context get() {
        return context.get();
    }

    public static void remove() {
        context.remove();
    }
}
