package com.msun.auth.custom.comdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageListDto<T> {
    private List<T> arr;
    private Number pageNo;
    private Number lastId;
    private Number pageSize;
    private Number total;
    private Object ext;
}
