package com.msun.auth.custom.comdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class DataBox {
    private Object data;

    private DataBox(Object data) {
        this.data = data;
    }

    public static DataBox of(Object data) {
        return new DataBox(data);
    }
}
