package com.msun.auth.custom.constants;

public final class CustomConsts {
    public static final String SIGN_KEY = "Fv46G5YGhNniPOq9";
    public static final String APP_ID = "appId";
    public static final String UID = "uid";
    public static final String UNAME = "uname";

    public static final String NAME = "name";
    public static final String GROUP_ID = "groupId";
    public static final String ROLE_LEVEL = "roleLevel";
    public static final String TOKEN = "token";


}
