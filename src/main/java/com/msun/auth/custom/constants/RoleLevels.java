package com.msun.auth.custom.constants;

/**
 * 类描述：
 *
 * @ClassName RoleLevels
 * @Description TODO
 * @Author gj
 * @Date 2023/8/19 13:04
 * @Version 1.0
 */

public final class RoleLevels {
    public static int SUPER_ADMIN = 10000;
    public static int GROUP_ADMIN = 1000;
    public static int APP_ADMIN = 100;
    public static int READ_WRITE = 10;
    public static int READ = 1;
}