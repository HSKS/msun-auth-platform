package com.msun.auth.custom.packdto;

import com.alibaba.fastjson.annotation.JSONField;
import com.msun.auth.custom.exception.BizResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "返回结果实体类", description = "结果实体类")
public class BizResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "返回码")
    private String code;
    @ApiModelProperty(value = "返回消息")
    private String message;
    @ApiModelProperty(value = "返回数据")
    private T data;
    @JSONField(serialize = false)
    @ApiModelProperty(value = "错误信息")
    private Throwable ex;


    public BizResponse(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public BizResponse<?> fail(BizResultCode code, Throwable e) {
        this.setCode(code.getCode());
        this.setMessage(code.getMessage());
        if (e != null) {
            this.setEx(e);
        }
        return this;
    }

    public BizResponse<?> failWithMessage(BizResultCode code, String message, Throwable e) {
        this.setCode(code.getCode());
        this.setMessage(message);
        if (e != null) {
            this.setEx(e);
        }
        return this;
    }
}
