package com.msun.auth.custom.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BizResultCode {
    Success("Success", "成功"),
    MustUpgrade("MustUpgrade", "强制客户端升级"),
    Error("Error", "服务器开小差，请稍后尝试～"),
    BadRequest("BadRequest", "无效请求"),
    ArgumentNotValid("ArgumentNotValid", "参数缺失"),
    BindException("BindException", "参数绑定异常"),
    MethodArgumentNotValidException("MethodArgumentNotValidException", "方法参数异常"),
    UnexpectedTypeException("UnexpectedTypeException", "不支持的类型"),
    HttpMessageNotReadableException("HttpMessageNotReadableException", "无法读取HTTP请求信息"),
    HttpRequestMethodNotSupportedException("HttpRequestMethodNotSupportedException", "HTTP METHOD错误"),
    IllegalArgumentException("IllegalArgumentException", "非法参数异常"),
    MissingServletRequestParameterException("MissingServletRequestParameterException", "缺失Servlet请求参数"),
    //业务错误
    ParseTokenInfoError("ParseTokenInfoError", "无效签名"),
    AccountNotExist("AccountNotExist", "账号不存在，请注册账号"),
    AccountExist("AccountNotExist", "账号名称已存在，请换一个账号名称"),
    NoRegisterType("NoRegisterType", "请选择注册方式"),
    NoMoney("NoMoney", "账户余额不足"),
    InvalidToken("InvalidToken", "无效签名"),
    InvalidRequest("InvalidRequest", "无效请求"),
    LiveRoomClosed("LiveRoomClosed", "直播已结束"),
    AccountKicked("AccountKicked", "您的账户已封禁"),
    AccountCanceled("AccountCanceled", "您的账号已注销"),
    SmsCodeInvalid("SmsCodeInvalid", "验证码已过期"),
    SmsCodeError("SmsCodeError", "验证码错误"),
    SecError("AuditError", "为了维护一个绿色网络环境，请不要发布违规内容哦～"),
    RecordNotExist("RecordNotExist", "无效记录"),
    NoRight("NoRight", "权限不足"),
    GoRecharge("GoRecharge", "该操作必须拥有VIP特权哦~"),

    InvalidRecharge("InvalidRecharge", "无效购买"),
    AuthByRestored("AuthByRestored", "您本机苹果支付ID已为其它账号认证过了，若要继续认证，请更换苹果支付ID进行支付。"),
    AppleIdBinded("AppleIdBinded", "您本机苹果支付ID已为其它账号购买过了，若要继续购买，请更换苹果支付ID进行支付。"),
    InBlacklist("InBlacklist", "黑名单成员无法发消息");

    private final String code;
    private final String message;
}
