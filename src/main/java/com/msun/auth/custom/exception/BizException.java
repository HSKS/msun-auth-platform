package com.msun.auth.custom.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BizException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String message;
    private Throwable e;
    private Object args;

    public BizException(BizResultCode code, Throwable e, Object... args) {
        this.code = code.getCode();
        this.e = e;
        this.message = code.getMessage();
        if (args != null && args.length > 0) {
            this.args = Arrays.asList(args);
        }
    }

    public BizException(BizResultCode code, Throwable e, String message, Object... args) {
        this.code = code.getCode();
        this.e = e;
        this.message = message;
        if (args != null && args.length > 0) {
            this.args = Arrays.asList(args);
        }
    }

    public BizException(BizResultCode code, String message, Object... args) {
        this.code = code.getCode();
        this.message = message;
        if (args != null && args.length > 0) {
            this.args = Arrays.asList(args);
        }
    }

    public BizException(BizResultCode code,  Object... args) {
        this.code = code.getCode();
        this.message = code.getMessage();
        if (args != null && args.length > 0) {
            this.args = Arrays.asList(args);
        }
    }


    public BizException(String code,  Throwable e) {
        this.code = code;
        this.message = e.getMessage();
        this.e = e;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
