package com.msun.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @Description //TODO
 * @Date ${DATE} ${TIME}
 * @Author guijie
 **/
@Slf4j
@EnableSwagger2WebMvc
@EnableAspectJAutoProxy
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        log.info("哈哈，俺老孙来也！！！");
    }
}